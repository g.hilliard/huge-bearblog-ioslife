+++
title = "{{ replace .Name "-" " " | title }}"
date = "{{ .Date }}"
rating = "?/10"

#
# description is optional
#
# description = "An optional description for SEO. If not provided, an automatically created summary will be used."

gameTrackingTags = []
+++

This is a page about »{{ replace .Name "-" " " | title }}« that I logged beaten on »{{ .Date }}«.
